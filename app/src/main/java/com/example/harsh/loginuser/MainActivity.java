package com.example.harsh.loginuser;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
    private static EditText user;
    private static EditText password;
    private static TextView Attempt;
    int attempt_count=5;
    private static Button button;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loginButton();
    }

    public void loginButton()
    {
        user=(EditText)findViewById(R.id.editText_user);
        password=(EditText)findViewById(R.id.editText_pass);
        Attempt=(TextView)findViewById(R.id.textView_attempt);
        button=(Button)findViewById(R.id.button_login);

        Attempt.setText(Integer.toString(attempt_count));

        button.setOnClickListener(
                new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        if(user.getText().toString().equals("User")&& password.getText().toString().equals("123456"))
                        {
                            Toast.makeText(MainActivity.this,"user name and password is correct",Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent("com.example.harsh.loginuser.MainActivity.User");
                                    startActivity(intent);

                        }
                        else
                        {
                            Toast.makeText(MainActivity.this,"user name and password is incorrect",Toast.LENGTH_SHORT).show();
                            attempt_count--;
                            Attempt.setText(Integer.toString(attempt_count));
                            if(attempt_count==0)
                            {
                                button.setEnabled(false);
                            }
                        }
                    }
                }
        );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
